# `EthSSH` - SSH into machines using Ethereum derived keys
To use this, you need a system-wide Web3 provider. Metamask and similar are
in-browser wallets and will not work. I personally use [frame.sh][1] and it
works great.

***Disclaimer: I'm not a cryptography expert. Don't use this for securing your
production systems. This is just an experiment, and probably has some way to
go to be considered "safe".***

## `ethsshctl` - A tool to manage ethssh keys
In order to use this, you need to derive a public key. That's what this program
is for. It can also add keys to the system ssh-agent.

### Machine-locked key
Machine-locked keys use a [system identifier][2] instead of a secret. It's not
impossible to derive them on another machine, but it's not recommended to use
this option if you want to derive the same key on multiple machines.

You can generate one of these by running 
```bash
$ ethsshctl derive machine --output="file_path" # write public key to file_path
$ ethsshctl derive machine # write public key to stdout
```
these will immediately prompt for a signature.

To add these keys, just run 
```bash
$ ethsshctl add machine --ttl=7200 # add key to ssh agent with 7200s lifetime
$ ethsshctl add machine # add key to ssh agent with default 3600s lifetime
```

### Floating key
Floating keys use a user-provided secret and can be derived anywhere as long as
the secret is re-entered on another machine. This can be slightly less secure,
but more convenient.

You can generate one of these by running 
```bash
$ ethsshctl derive floating --output="file_path" # write public key to file_path
$ ethsshctl derive floating # write public key to stdout
```
these will prompt you to type your secret key twice and then will derive the
key, prompting a signature.

To add these keys, just run 
```bash
$ ethsshctl add floating --ttl=7200 # add key to ssh agent with 7200s lifetime
$ ethsshctl add floating # add key to ssh agent with default 3600s lifetime
```

## Technical Details
Key pairs are derived deterministically by using consistent steps. 
- Create some nonce from either machine ID or user-provided secret
- Generate a signature on that nonce from your signer
- Use a KDF to derive a seed key from that signature and your provided nonce
- Use the derived seed key as the input to an ed25519 key generator. 

Each step is deterministic, repeatable, and does not expose your Ethereum
private key.

In theory, the output of the KDF could be used to create any type of key, I
chose ed25519.

[1]: https://frame.sh
[2]: https://github.com/denisbrodbeck/machineid
