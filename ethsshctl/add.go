package main

import "github.com/spf13/cobra"

var (
	keyLifetime uint32
)

var addSubcmd = &cobra.Command{
	Use:   "add",
	Short: "Adds keys to ssh-agent",
}

func init() {
	addSubcmd.PersistentFlags().Uint32Var(&keyLifetime, "lifetime", 3600,
		"TTL in secondsof key in ssh-agent, default 3600")
	RootCmd.AddCommand(addSubcmd)
}
