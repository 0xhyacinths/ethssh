package main

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/0xhyacinths/ethssh/lib/ethssh"
)

var deriveLockedCmd = &cobra.Command{
	Use:   "machine",
	Short: "Derive a machine-locked key",
	RunE: func(cmd *cobra.Command, args []string) error {
		nonce, err := ethssh.CreateMachineNonce()
		if err != nil {
			return err
		}
		key, err := deriveKey(context.Background(), nonce, "machine")
		if err != nil {
			return err
		}
		err = writeOrPrintKey(key)
		if err != nil {
			return err
		}
		return nil
	},
}

func init() {
	deriveSubcmd.AddCommand(deriveLockedCmd)
}
