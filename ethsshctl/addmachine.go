package main

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/0xhyacinths/ethssh/lib/ethssh"
)

var addLockedCmd = &cobra.Command{
	Use:   "machine",
	Short: "Add a machine-locked key to the ssh agent",
	RunE: func(cmd *cobra.Command, args []string) error {
		nonce, err := ethssh.CreateMachineNonce()
		if err != nil {
			return err
		}

		err = insertKey(context.Background(), nonce, keyLifetime)
		if err != nil {
			return err
		}
		return nil
	},
}

func init() {
	addSubcmd.AddCommand(addLockedCmd)
}
