package main

import (
	"context"
	"errors"
	"fmt"
	"strings"
	"syscall"

	"github.com/spf13/cobra"
	"gitlab.com/0xhyacinths/ethssh/lib/ethssh"
	"golang.org/x/term"
)

var addFloatingCmd = &cobra.Command{
	Use:   "floating",
	Short: "Add a floating key to the SSH agent",
	RunE: func(cmd *cobra.Command, args []string) error {
		fmt.Print("Enter secret: ")
		secret1, err := term.ReadPassword(int(syscall.Stdin))
		if err != nil {
			return err
		}
		fmt.Println()
		fmt.Print("Enter secret again: ")
		secret2, err := term.ReadPassword(int(syscall.Stdin))
		if err != nil {
			return err
		}
		fmt.Println()

		if strings.TrimSpace(string(secret1)) != strings.TrimSpace(string(secret2)) {
			return errors.New("secrets don't match")
		}

		nonce, err := ethssh.CreateSecretNonce(strings.TrimSpace(string(secret1)))
		if err != nil {
			return err
		}
		err = insertKey(context.Background(), nonce, keyLifetime)
		if err != nil {
			return err
		}
		return nil
	},
}

func init() {
	addSubcmd.AddCommand(addFloatingCmd)
}
