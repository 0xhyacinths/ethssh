package main

import (
	"log"
	"os"
	"path"

	"github.com/spf13/cobra"
)

var (
	configFilePath string
	outputPath     string
)

var deriveSubcmd = &cobra.Command{
	Use:   "derive",
	Short: "Derives ssh keys from eth signatures",
}

func init() {
	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}
	defaultPath := path.Join(home, "/.ssh/ethsshd.json")
	deriveSubcmd.PersistentFlags().StringVar(&configFilePath, "config", defaultPath,
		"ethsshd config file (default is ~/.ssh/ethsshd.json)")
	deriveSubcmd.PersistentFlags().StringVar(&outputPath, "output", "",
		"output public key path (default is stdout)")

	RootCmd.AddCommand(deriveSubcmd)

}
