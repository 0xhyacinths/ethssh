package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/0xhyacinths/ethssh/lib/ethssh"
	"golang.org/x/crypto/ssh/agent"
)

var (
	providerUri string
)

var RootCmd = &cobra.Command{
	Use:   "ethsshctl",
	Short: "The root of ethsshctl",
}

func main() {
	if err := RootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	RootCmd.PersistentFlags().StringVar(&providerUri, "provider",
		"http://127.0.0.1:1248",
		"provider URI (default is http://127.0.0.1:1248)")
}

func readConfigFile(path string) ([]*ethssh.Config, error) {
	_, err := os.Stat(path)

	if errors.Is(err, os.ErrNotExist) {
		return []*ethssh.Config{}, nil
	} else if err != nil {
		return nil, err
	}

	return ethssh.ReadConfiguration(path)
}

func createOrOverwriteKey(cfgs []*ethssh.Config, insert *ethssh.Config) []*ethssh.Config {
	for i, cfg := range cfgs {
		if cfg.Account.String() == insert.Account.String() {
			cfgs[i] = insert
			return cfgs
		}
	}
	cfgs = append(cfgs, insert)
	return cfgs
}

func insertKey(ctx context.Context, nonce []byte, expire uint32) error {
	es := ethssh.NewEthSSH(providerUri)
	accounts, err := es.Accounts(ctx)
	if err != nil {
		return err
	}
	if len(accounts) < 1 {
		return errors.New("enumerated no accounts")
	}
	account := accounts[0]

	skey, err := es.PrivateKey(ctx, account, nonce)
	if err != nil {
		return err
	}

	sshagent, err := connectToAgent()
	if err != nil {
		return err
	}

	err = sshagent.Add(agent.AddedKey{
		PrivateKey:   skey,
		Comment:      fmt.Sprintf("ethssh key for %s", account.String()),
		LifetimeSecs: expire,
	})
	if err != nil {
		return err
	}

	return nil
}

func connectToAgent() (agent.ExtendedAgent, error) {
	path := os.Getenv("SSH_AUTH_SOCK")
	if path == "" {
		return nil, errors.New("no ssh sock")
	}
	conn, err := net.Dial("unix", path)
	if err != nil {
		return nil, err
	}
	return agent.NewClient(conn), nil
}

func deriveKey(ctx context.Context, nonce []byte, ktype string) ([]byte, error) {
	cfg, err := readConfigFile(configFilePath)
	if err != nil {
		return nil, err
	}

	es := ethssh.NewEthSSH(providerUri)
	accounts, err := es.Accounts(ctx)
	if err != nil {
		return nil, err
	}

	if len(accounts) < 1 {
		return nil, errors.New("enumerated no accounts")
	}
	account := accounts[0]

	skey, err := es.PrivateKey(ctx, account, nonce)
	if err != nil {
		return nil, err
	}

	pubkey, err := ethssh.MarshalPublicKey(skey.Public())
	if err != nil {
		return nil, err
	}

	temp := &ethssh.Config{
		Account: account,
		Public:  strings.Trim(string(pubkey), "\n"),
		Type:    ktype,
	}
	cfgs := createOrOverwriteKey(cfg, temp)

	err = ethssh.WriteConfiguration(configFilePath, cfgs)
	if err != nil {
		return nil, err
	}
	return pubkey, nil
}

func writeOrPrintKey(key []byte) error {
	if outputPath == "" {
		fmt.Println(string(key))
		return nil
	}

	f, err := os.OpenFile(outputPath, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write(key)
	if err != nil {
		return err
	}
	return nil
}
