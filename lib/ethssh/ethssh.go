package ethssh

import (
	"context"
	"crypto"
	"crypto/ed25519"

	"github.com/ethereum/go-ethereum/common"
	"gitlab.com/0xhyacinths/ethssh/lib/web3"
	"golang.org/x/crypto/argon2"
	"golang.org/x/crypto/ssh"
)

type EthSSH struct {
	web3 *web3.Provider
}

func NewEthSSH(endpoint string) *EthSSH {
	prov := web3.NewProvider(endpoint, "EthSSH")
	return &EthSSH{
		web3: prov,
	}
}

func (e *EthSSH) Accounts(ctx context.Context) ([]common.Address, error) {
	return e.web3.Accounts(ctx)
}

func (e *EthSSH) PrivateKey(ctx context.Context, addr common.Address, nonce []byte) (ed25519.PrivateKey, error) {
	sig, err := e.web3.Sign(ctx, addr, nonce)
	if err != nil {
		return nil, err
	}

	// Salt the derived key with the address it's derived from.
	// Is this safe? I'm not a cryptography expert here.
	derivation := argon2.IDKey(sig, addr.Bytes(), 1, 64*1024, 4, 32)
	privateKey := ed25519.NewKeyFromSeed(derivation)
	return privateKey, nil
}

func MarshalPublicKey(key crypto.PublicKey) ([]byte, error) {
	publicKey, err := ssh.NewPublicKey(key)
	if err != nil {
		return nil, err
	}

	authorizedKey := ssh.MarshalAuthorizedKey(publicKey)
	return authorizedKey, nil
}
