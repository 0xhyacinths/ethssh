package ethssh

import (
	"encoding/hex"
	"fmt"

	"github.com/denisbrodbeck/machineid"
	"golang.org/x/crypto/sha3"
)

// CreateMachineNonce creates a nonce for a machine-locked key based on the
// hostname and username of the key.
func CreateMachineNonce() ([]byte, error) {
	id, err := machineid.ID()
	if err != nil {
		return nil, err
	}
	message := []byte(fmt.Sprintf("machlocked_ssh_%s", id))
	return message, nil
}

// CreateSecretNonce creates a nonce derived from some secret provided by the
// user.
func CreateSecretNonce(secret string) ([]byte, error) {
	h := sha3.NewLegacyKeccak256()
	_, err := h.Write([]byte(secret))
	if err != nil {
		return nil, err
	}
	hexHash := hex.EncodeToString(h.Sum(nil))
	message := []byte(fmt.Sprintf("floating_ssh_%s", hexHash))
	return message, nil
}
