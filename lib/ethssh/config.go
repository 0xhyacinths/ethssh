package ethssh

import (
	"encoding/json"
	"io"
	"os"

	"github.com/ethereum/go-ethereum/common"
)

type Config struct {
	Account common.Address `json:"account"`
	Public  string         `json:"public"`
	Type    string         `json:"type"`
}

func ReadConfiguration(path string) ([]*Config, error) {
	f, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	b, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	var cfg []*Config
	err = json.Unmarshal(b, &cfg)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}

func WriteConfiguration(path string, cfg []*Config) error {
	b, err := json.MarshalIndent(cfg, "", "  ")
	if err != nil {
		return err
	}

	f, err := os.OpenFile(path, os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write(b)
	if err != nil {
		return err
	}
	return nil
}
