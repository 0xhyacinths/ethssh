package web3

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHeader(t *testing.T) {
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			t.Fatal(err)
		}
		hval, ok := r.Header["Origin"]
		if !ok {
			t.Fatal("Origin header not set")
		}

		if hval[0] != "TestEth" {
			t.Fatalf("Expected TestEth, got %s", hval[0])
		}

		if string(b) == "{\"method\":\"eth_accounts\",\"id\":0,\"jsonrpc\":\"2.0\"}" {
			w.Write([]byte("{\"id\":1,\"jsonrpc\":\"2.0\",\"result\":[\"0x0123456789abcdef0123456789abcdef12345678\"]}"))
		}
	}))

	prov := NewProvider(svr.URL, "TestEth")
	_, err := prov.Accounts(context.Background())
	if err != nil {
		t.Fatal(err)
	}
}
