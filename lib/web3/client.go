package web3

import (
	"context"

	"github.com/ethereum/go-ethereum/common"
)

func (p *Provider) Accounts(ctx context.Context) ([]common.Address, error) {
	var retval []common.Address
	err := p.client.CallFor(ctx, &retval, "eth_accounts")
	if err != nil {
		return nil, err
	}
	return retval, nil
}
