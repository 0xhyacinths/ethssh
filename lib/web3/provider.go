package web3

import (
	"github.com/ybbus/jsonrpc/v3"
)

type Provider struct {
	client jsonrpc.RPCClient
}

func NewProvider(endpoint, name string) *Provider {
	opts := &jsonrpc.RPCClientOpts{
		CustomHeaders: map[string]string{
			"Origin": name,
		},
	}
	rpcClient := jsonrpc.NewClientWithOpts(endpoint, opts)
	return &Provider{
		client: rpcClient,
	}
}
