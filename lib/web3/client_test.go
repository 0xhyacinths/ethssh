package web3

import (
	"context"
	"io"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/ethereum/go-ethereum/common"
)

func TestAccounts(t *testing.T) {
	svr := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			t.Fatal(err)
		}
		if string(b) == "{\"method\":\"eth_accounts\",\"id\":0,\"jsonrpc\":\"2.0\"}" {
			w.Write([]byte("{\"id\":1,\"jsonrpc\":\"2.0\",\"result\":[\"0x0123456789abcdef0123456789abcdef12345678\"]}"))
		}
	}))

	prov := NewProvider(svr.URL, "TestEth")
	addrs, err := prov.Accounts(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	expected := []common.Address{
		common.HexToAddress("0x0123456789abcdef0123456789abcdef12345678"),
	}

	if !reflect.DeepEqual(addrs, expected) {
		t.Fatalf("Expected %v got %v", expected, addrs)
	}
}
