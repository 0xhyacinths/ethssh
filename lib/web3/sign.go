package web3

import (
	"context"
	"encoding/hex"

	"github.com/ethereum/go-ethereum/common"
)

func (p *Provider) Sign(ctx context.Context, account common.Address, message []byte) ([]byte, error) {
	var temp string
	err := p.client.CallFor(ctx, &temp, "eth_sign", account, hex.EncodeToString(message))
	if err != nil {
		return nil, err
	}
	data, err := hex.DecodeString(temp[2:])
	if err != nil {
		return nil, err
	}
	return data, nil
}
