module gitlab.com/0xhyacinths/ethssh

go 1.18

require (
	github.com/denisbrodbeck/machineid v1.0.1
	github.com/ethereum/go-ethereum v1.10.19
	github.com/spf13/cobra v1.5.0
	github.com/ybbus/jsonrpc/v3 v3.1.0
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
	golang.org/x/term v0.0.0-20201126162022-7de9c90e9dd1
)

require (
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.0.0-20211019181941-9d821ace8654 // indirect
)
